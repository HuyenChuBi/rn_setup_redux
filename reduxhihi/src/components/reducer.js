import { UP, DOWN } from './action'
const defaultState = {
    value: 10,
    hoa: 'hoa hong'
};
export const reducer = (state = defaultState, action) => {
    if (action.type == UP) { return { value: state.value + 1 } }
    if (action.type == DOWN) { return { value: state.value - 1 } }
    return state;
}