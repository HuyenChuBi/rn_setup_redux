import { createStore } from "redux";
import { reducer } from './reducer'
const defaultState = {
    value: 10,
    hoa: 'hoa hong'
};

export const store = createStore(reducer)
