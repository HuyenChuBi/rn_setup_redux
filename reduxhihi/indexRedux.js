import { createStore } from "redux";

//Tap redux store don gian
const defaultState = { value: 0 };
const reducer = (state = defaultState, action) => {
    if (action.type === "UP") return { value: state.value + 1 }
    if (action.type === "DOWN") return { value: state.value - 1 }
    return state;
}
const store = createStore(reducer)

