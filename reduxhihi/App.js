/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Controller from './src/components/Controller';
import ViewActor from './src/components/ViewActor'
import { Provider } from 'react-redux'
import { createStore } from "redux";

import { store } from './src/components/store'

export default class App extends Component {
  render() {
    return (
      <View style={styleApp.container}>
        <Provider store={store}>
          <ViewActor />
          <Controller />
        </Provider>
      </View>

    );
  }
}

const styleApp = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    paddingTop: 30
  },
  header: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  appName: {
    color: 'white',
    fontSize: 30,
    textAlign: 'center'
  },
  value: {
    color: 'yellow',
    fontSize: 40
  }
});

const styleController = StyleSheet.create({
  controller: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'yellow',
    alignSelf: 'stretch',
    margin: 20
  },
  controllName: {
    fontSize: 20,
    marginBottom: 10
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  button: {
    backgroundColor: 'black',
    paddingHorizontal: 50,
    paddingVertical: 25,
    margin: 10,
    borderRadius: 5
  },
  buttonText: {
    color: 'white',
    fontSize: 40
  }
});


